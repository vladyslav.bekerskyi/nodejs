const express = require('express')

const router = express.Router()
const {createFile,getFile,getFiles}=require('../controllers/fileController')


router.get('/',getFiles)
router.post('/',createFile)
router.get('/:filename',getFile)


module.exports = router

