const express = require('express');
const cors = require('cors');
const morgan = require('morgan')
const filesRoute = require('./routes/filesRoute')
const createFolder = require('./middleware/existMiddleware')

const app = express();
const PORT = process.env.PORT || 8080


app.use(morgan(':method :status :url  :response-time ms'))
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({
    extended: true
}));
app.use(createFolder)

app.use('/api/files',filesRoute)

app.listen(PORT, () => {
    console.log(`Server is running on port ${PORT}...`);
})








