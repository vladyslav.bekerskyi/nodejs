const fs = require('fs')
const path = require('path')
const emptyFolder = path.join(__dirname,'..', 'files')

const createFolder = (req,res,next)=> {
  if (!fs.existsSync(emptyFolder)) {
    fs.mkdirSync(emptyFolder)
  }
  next()
}

module.exports = createFolder