const getExtension = require("../utils/getExtension");
const ifExists = require("../utils/ifExists");
const create = require("../utils/create");
const arrOfFiles = require("../utils/arrOfFiles");
const getContent = require('../utils/getContent');
const getDate = require('../utils/getDate');

const extensions = ['log', 'txt', 'json', 'yaml', 'xml', 'js'];

const createFile = (req, res) => {
  try {
    if (!("content" in req.body) || !("filename" in req.body)) {
      return res.status(400).json({
        message: "Please specify 'content' parameter",
      });
    }

    if (ifExists(extensions, getExtension(req.body.filename)) === false) {
      return res.status(400).json({
        message: "Application support log, txt, json, yaml, xml, js file extensions",
      });
    }

    if (ifExists(arrOfFiles(), req.body.filename)) {
      return res.status(400).json({
        message: "File exists",
      });
    }

    create(req.body.filename, req.body.content);

    res.status(200).json({
      message: "File created successfully",
    });
  } catch (error) {
    return res.status(500).json({
      message: "Server error"
    });
  }
}

const getFiles = (req, res) => {
  try {
    if (arrOfFiles().length === 0) {
      return res.status(400).json({
        message: 'Client error',
      });
    }

    res.status(200).json({
      message: 'Success',
      files: arrOfFiles(),
    });
  } catch (error) {
    return res.status(500).json({
      message: 'Server error',
    });
  }
}

const getFile = (req, res) => {
  try {
    if (!ifExists(arrOfFiles(), req.params.filename)) {
      return res.status(400).json({
        message: `No file with '${req.params.filename}' filename found`,
      });
    }
    res.status(200).json({
      message: 'Success',
      filename: req.params.filename,
      content: getContent(req.params.filename),
      extension: getExtension(req.params.filename),
      uploadedDate: getDate(req.params.filename).birthtime,
    });
  } catch (error) {
    res.status(500).json({
      message: 'Server error',
    });
  }
}

module.exports = {
  createFile,
  getFile,
  getFiles
}