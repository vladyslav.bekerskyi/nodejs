const fs = require('fs');
const path = require('path');

const directoryPath = path.join(__dirname, '..', 'files');

const getContent = fileName =>
    fs.readFileSync(path.join(directoryPath, fileName), 'utf8', err => {
        if (err) console.error(err);
    });

module.exports = getContent;
