const path = require('path');
const fs = require('fs');

const create = (fileName, fileContent) => {
    try {
        fs.writeFileSync(path.join('files', fileName), fileContent);
    } catch (err) {
        throw err;
    }
};

module.exports = create;
