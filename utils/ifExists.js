const ifExists = (arr, el) => arr.includes(el);

module.exports = ifExists;