const fs = require('fs');
const path = require('path');

const directoryPath = path.join(__dirname, '..', 'files');

const getArrOfFilesInDir = () =>
    fs.readdirSync(directoryPath, err => {
        if (err) throw err;
    });

module.exports = getArrOfFilesInDir;
